# JWT Rest
> API-REST JWT auth

Creates a Go Api Rest using JWT authentication.

This API provides the following endpoints:
* /login POST - Logs a user in and returns the JWT.
* /private GET - Returns a private info only for logged people.