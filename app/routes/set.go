/*
   set.go function to set router handlers
   Copyright (C) 2019 Bruno Mondelo Giaramita

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package routes

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/mondelob/jwt-rest/app/controllers"
)

func SetRoutes(router *mux.Router) {
	setRoutesGet(router)
	setRoutesPost(router)
}

func setRoutesGet(router *mux.Router) {
}

func setRoutesPost(router *mux.Router) {
	post(router, "/login", controllers.HandlerLogin)
}

func setRoute(router *mux.Router, path string, f func(w http.ResponseWriter,
	r *http.Request), method string) {
	router.HandleFunc(path, f).Methods(method)
}
