/*
   login.go function to define the application routes handlers
   Copyright (C) 2019 Bruno Mondelo Giaramita

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package controllers

import (
	"net/http"
	"os"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

/* Expiration time of JWT in seconds */
const expirationSeconds time.Duration = 900

/* Set signing method */
var defaultSigningMethod = jwt.SigningMethodHS256

func HandlerLogin(w http.ResponseWriter, r *http.Request) {
	/* check user login */
	w.Write([]byte(makeJWT("name")))
}

func makeJWT(name string) string {
	var jwtSigningMethod *jwt.SigningMethodHMAC
	signingMethod := os.Getenv("JWT_SIGN_METHOD")

	if signingMethod == "" {
		jwtSigningMethod = defaultSigningMethod
	} else if signingMethod == "HS256" {
		jwtSigningMethod = jwt.SigningMethodHS256
	}

	token := jwt.New(jwtSigningMethod)

	claims := token.Claims.(jwt.MapClaims)

	claims["iss"] = "gitlab.com/mondelob/jwt-rest"
	claims["name"] = name
	claims["exp"] = time.Now().Add(time.Second * expirationSeconds).Unix()

	tokenString, _ := token.SignedString([]byte(os.Getenv("JWT_SECRET_KEY")))

	return tokenString
}
