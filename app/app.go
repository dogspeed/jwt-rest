/*
   app.go defines the application
   Copyright (C) 2019 Bruno Mondelo Giaramita

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package app

import (
	"log"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/mondelob/jwt-rest/app/routes"
)

type App struct {
	Router *mux.Router
	/* DB handler */
}

func NewApp() *App {
	/* Configure and open database handler */

	app := new(App)

	app.Router = mux.NewRouter().StrictSlash(true)

	routes.SetRoutes(app.Router)

	return app
}

func (app *App) Serve(port string) {
	log.Fatal(http.ListenAndServe(":"+port, handlers.LoggingHandler(os.Stdout,
		app.Router)))
}
