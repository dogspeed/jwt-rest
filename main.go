/*
   jwt_rest.go an API with JWT auth
   Copyright (C) 2019 Bruno Mondelo Giaramita

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/mondelob/jwt-rest/app"
)

/* Environment file path */
const envFile string = "app.env"

func main() {
	err := godotenv.Load(envFile)
	if err != nil {
		log.Fatal("error loading environment")
	}

	port := os.Getenv("PORT")

	app := app.NewApp()

	fmt.Println("JWT Auth API serving on port " + port)

	app.Serve(port)
}
